import React from "react";

import Header from "../components/Header/index";

const HomepageLayout = (props) => {
  return (
    <div className="fullHeight">
      <Header />
      {props.children}
    </div>
  );
};

export default HomepageLayout;
